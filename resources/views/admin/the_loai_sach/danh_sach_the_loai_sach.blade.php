/@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thể loại sách
                            <small>Danh sách thể loại sách</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã thể loại sách</th>
                                <th>Tên thể loại sách</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($the_loai_sach as $tl)
                            <tr class="even gradeC" align="center">
                                <td>{{$tl->ma_the_loai_sach}}</td>
                                <td>{{$tl->ten_the_loai_sach}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/the_loai_sach/sua_the_loai_sach/{{$tl->ma_the_loai_sach}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/the_loai_sach/xoa_the_loai_sach/{{$tl->ma_the_loai_sach}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection