@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ngành
                            <small>Danh sách ngành</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Ngành</th>
                                <th>Tên Ngành</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nganh as $nganh)
                            <tr class="even gradeC" align="center">
                                <td>{{$nganh->ma_nganh}}</td>
                                <td>{{$nganh->ten_nganh}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"><a href="tong/nganh/sua_nganh/{{$nganh->ma_nganh}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i></i> <a href="tong/nganh/xoa_nganh/{{$nganh->ma_nganh}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection