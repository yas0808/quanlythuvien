<html lang="en">

<head>
  <title>{{$xem_chi_tiet_sach->ten_sach}}</title>
  <link href="https://fonts.googleapis.com/css?family=Bentham|Playfair+Display|Raleway:400,500|Suranna|Trocchi" rel="stylesheet">
  <style type="text/css">
  	body {
  background-color: #fdf1ec;
}

.wrapper {
  height: 500px;
  width: 10000px;
  margin: 50px auto;
  border-radius: 7px 7px 7px 7px;
  /* VIA CSS MATIC https://goo.gl/cIbnS */
  -webkit-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
  -moz-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
  box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
}

.product-img {
  float: left;
  height: 420px;
  width: 327px;
}

.product-img img {
  border-radius: 7px 0 0 7px;
}

.product-info {
  float: left;
  height: 400px;
  width: 800px;
  border-radius: 0 3px 10px 7px;
  background-color: #ffffff;
}

.product-text {
  height: 100px;
  width: 800px;
}

.product-text h1 {
  margin: 0 0 0 250px;
  padding-top: 50px;
  font-size: 34px;
  color: #474747;
}

.product-text h1,
.product-price-btn p {
  font-family: 'Bentham', serif;
}

.product-text h2 {
  margin: 10 20px 3px 290px;
  font-size: 13px;
  font-family: 'Raleway', sans-serif;
  font-weight: 300;
  text-transform: uppercase;
  color: #black;
  letter-spacing: 0.2em;
}

.product-text p {
  height: 300px;
  margin: 0.2 0 0 25px;
  font-family: 'Playfair Display', serif;
  color: #black;
  line-height: 1.7em;
  font-size: 18px;
  font-weight: lighter;
  overflow: hidden;
}

.product-price-btn {
  height: 103px;
  width: 327px;
  margin-top: 17px;
  position: relative;
}

.product-price-btn p {
  display: inline-block;
  position: absolute;
  top: 70px;
  height: 50px;
  font-family: 'Trocchi', serif;
  margin: 0 0 0 38px;
  font-size: 30px;
  font-weight: lighter;
  color: #474747;
}

span {
  display: inline-block;
  height: 50px;
  font-family: 'Suranna', serif;
  font-size: 34px;
}

.product-price-btn button {
  float: right;
  display: inline-block;
  height: 50px;
  width: 176px;
  margin: 0 40px 0 16px;
  box-sizing: border-box;
  border: transparent;
  border-radius: 60px;
  font-family: 'Raleway', sans-serif;
  font-size: 10px;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 0.2em;
  color: #ffffff;
  background-color: #9cebd5;
  cursor: pointer;
  outline: none;
}

.product-price-btn button:hover {
  background-color: #79b0a1;
}
  </style>
</head>

<body>
  <div class="wrapper">
    <div class="product-img">
      <img src="http://bit.ly/2tMBBTd" height="420" width="327">
    </div>
    <div class="product-info">
      <div class="product-text">
        <h1>{{$xem_chi_tiet_sach->ten_sach}}</h1>
        <h2>{{$xem_chi_tiet_sach->tac_gia->ten_tac_gia}}</h2>
        <p>
        <br>Thể loại: {{$xem_chi_tiet_sach->the_loai_sach->ten_the_loai_sach}} </br>
        <br>Nhà xuất bản: {{$xem_chi_tiet_sach ->nha_xuat_ban['ten_nha_xuat_ban']}}</br>
        <br>Giới thiệu: {{$xem_chi_tiet_sach->gioi_thieu}} </br>
        </p>
        
      </div>
     </div>
      <div class="product-price-btn">
        <p><span>{{number_format($xem_chi_tiet_sach->gia_tien)}} VNĐ</span></p>
  	
      </div>
    </div>
  </div>

</body>

</html>