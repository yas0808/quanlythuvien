@extends('doc_gia.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách 
                            <small>Kho Sách</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                
                                <th>Thể loại sách</th>
                                <th>Tác Giả</th>
                                <th>Nhà Xuất Bản</th>
                                <th>Tên Sách</th>
                                <th>Giới Thiệu</th>


                                <th>Giá Tiền</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($sach as $sh)
                            <tr class="even gradeC" align="center">
                                <td>{{$sh->the_loai_sach->ten_the_loai_sach}}</td>
                                <td>{{$sh->tac_gia->ten_tac_gia}}</td>
                                <td>{{$sh->nha_xuat_ban['ten_nha_xuat_ban']}}</td>
                                <td>{{$sh->ten_sach}}</td>
                                <td>{{$sh->gioi_thieu}}</td>

                                <td>
                                    <?php echo number_format($sh->gia_tien); ?>
                                </td>
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection