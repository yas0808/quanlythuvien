@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tác Giả
                            <small>Danh sách Tác Giả</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Tác Giả</th>
                                <th>Tên Tác Giả</th>   
                                <th>Giới Thiệu</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($tac_gia as $tg)
                            <tr class="even gradeC" align="center">
                                <td>{{$tg->ma_tac_gia}}</td>
                                <td>{{$tg->ten_tac_gia}}</td>
                                <td>{{$tg->gioi_thieu}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/tac_gia/sua_tac_gia/{{$tg->ma_tac_gia}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/tac_gia/xoa_tac_gia/{{$tg->ma_tac_gia}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection