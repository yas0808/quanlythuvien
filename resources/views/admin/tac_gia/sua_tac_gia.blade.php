@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Tác Giả
                            <small>{{$tac_gia -> ten_tac_gia}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/tac_gia/sua_tac_gia/{{$tac_gia->ma_tac_gia}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên Tác Giả</label>
                                <input class="form-control" name="ten_tac_gia" placeholder="Nhập Tên Tác Giả" value="{{$tac_gia->ten_tac_gia}}" />
                            </div>
                            <div class="form-group">
                                <label>Giới Thiệu</label>
                                <input class="form-control" name="gioi_thieu" placeholder="Nhập Giới Thiệu về Tác Giả" value="{{$tac_gia->gioi_thieu}}" />
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



