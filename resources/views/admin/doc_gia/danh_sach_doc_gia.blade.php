@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Độc Giả
                            <small>Danh Sách Độc Gỉa</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Ngành</th>
                                <th>Tên độc giả</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                               
                                <th>Cấp độ</th>
                                 <th>SĐT</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($doc_gia as $dg)
                            <tr class="even gradeC" align="center">
                                
                                <td>{{$dg->nganh->ten_nganh}}</td>
                                <td>{{$dg->ten_doc_gia}}</td>
                                <td>{{$dg->email}}</td>
                                
                                <td>{{$dg->dia_chi}}</td>
                                <td>{{$dg->ngay_sinh}}</td>
                                <td>@if($dg->gioi_tinh == 1)
                                    {{"Nữ"}}
                                    @else
                                    {{"Nam"}}
                                    @endif</td>
                              
                                <td>
                                    @if($dg->cap_do == 1)
                                    {{"sinh viên"}}
                                    @else
                                    {{"Giảng Viên"}}
                                    @endif
                                </td>
                                <td>
                                    {{$dg->sdt}}
                                </td>
                                
                                 <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="tong/doc_gia/sua_doc_gia/{{$dg->ma_doc_gia}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i> <a href="tong/doc_gia/xoa_doc_gia/{{$dg->ma_doc_gia}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection