@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Độc Giả
                            <small>{{$doc_gia -> ten_doc_gia}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/doc_gia/sua_doc_gia/{{$doc_gia->ma_doc_gia}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                           <div class="form-group">
                            <label>Ngành</label>
                            <select class="form-control" name="nganh">
                            @foreach ($nganh as $ng)
                                @if($doc_gia->ma_nganh == $ng->ma_doc_gia)
                                {{"selected"}}
                                @endif
                                <option value="{{ $ng->ma_nganh }}">
                                    {{$ng->ten_nganh}}
                                </option>
                            @endforeach    
                            </select>
                        </div>

                            <div class="form-group">
                                <label>Tên Độc Giả</label>
                                <input class="form-control" name="ten_doc_gia" placeholder="Nhập tên độc giả" value="{{$doc_gia->ten_doc_gia}}" />
                            </div>
                             <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" 
                                value="{{$doc_gia->email}}"  />
                            </div>
                             <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control " name="dia_chi" placeholder="Nhập địa chỉ"
                                 value="{{$doc_gia->dia_chi}}"/>
                            </div>
                             <div class="form-group">
                                <label>Ngày Sinh</label>
                                <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" value="{{$doc_gia->ngay_sinh}}"/>
                            </div>
                            
                           
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
                             <div class="form-group">
                                <label>Cấp Đọ</label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="0" checked="" type="radio">giảng viên
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">sinh viên
                                </label>
                            </div>
                             <div class="form-group">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại"
                                value="{{$doc_gia->sdt}}" />
                            </div>
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



