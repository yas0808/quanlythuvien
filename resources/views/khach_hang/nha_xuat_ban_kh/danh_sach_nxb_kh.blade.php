@extends('khach_hang.layout.index')

@section('content')
 <!-- Page Content -->
        
                <div class="row">
                    <div class="col-lg-12">
                       <marquee><h1 class="page-header" style="color: #000066"> Danh Sách Nhà Xuất bản
                        </h1>
                    </marquee>
                    </div>
                    <br>
                   
                  <div align="center">
                    <form style="width: 55%; height: 400px"  >
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center" style="background-color:#FFCC66">
                               
                                <th>Tên nhà xuất bản</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nha_xuat_ban as $xb)
                            <tr class="even gradeC" align="center" style="background-color: #FFCC99">
                                
                                <td>{{$xb->ten_nha_xuat_ban}}</td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection