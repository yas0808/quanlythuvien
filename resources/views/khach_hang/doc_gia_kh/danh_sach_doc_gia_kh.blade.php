@extends('khach_hang.layout.index')

@section('content')
 <!-- Page Content -->
        
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" align="center" style="color: #FF6600">Độc Giả
                            <small style="color: #FF9933">Danh Sách Độc Giả</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                   </div >
                    <div align="center">
                    <form style="width: 95%; height: 400px"  >
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Ngành</th>
                                <th>Tên độc giả</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                               
                                <th>Cấp độ</th>
                                 <th>SĐT</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($doc_gia as $dg)
                            <tr class="even gradeC" align="center">
                                
                                <td>{{$dg->nganh->ten_nganh}}</td>
                                <td>{{$dg->ten_doc_gia}}</td>
                                <td>{{$dg->email}}</td>
                                
                                <td>{{$dg->dia_chi}}</td>
                                <td>{{$dg->ngay_sinh}}</td>
                                <td>@if($dg->gioi_tinh == 1)
                                    {{"Nữ"}}
                                    @else
                                    {{"Nam"}}
                                    @endif</td>
                              
                                <td>
                                    @if($dg->cap_do == 1)
                                    {{"sinh viên"}}
                                    @else
                                    {{"Giảng Viên"}}
                                    @endif
                                </td>
                                <td>
                                    {{$dg->sdt}}
                                </td>
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </form>
                </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection