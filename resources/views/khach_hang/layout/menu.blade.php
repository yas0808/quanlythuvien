<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        /*==Reset CSS==*/
* {
  margin: 0;
  padding: 0;
}

/*==Style cơ bản cho website==*/
body {
  font-family: sans-serif;
  color: #333;
}

/*==Style cho menu===*/
#menu ul {
  background: #1F568B;
  list-style-type: none;
  text-align: center;
}
#menu li {
  color: #f1f1f1;
  display: inline-block;
  width: 200px;
  height: 50px;
  line-height: 40px;
  margin-left: -5px;
}
#menu a {
  text-decoration: none;
  color: #fff;
  display: block;
}
#menu a:hover {
  background: #F1F1F1;
  color: #333;
}

    </style>
</head>
<body>
    <div id="menu">
  <ul>
    <li><a href="khach_hang/trang_chu">Trang chủ</a></li>
    <li><a href="khach_hang/danh_sach_sach_kh">Sách</a></li>
    <li><a href="khach_hang/danh_sach_tls_kh">Danh Sách Thể loại sách</a></li>
    <li><a href="khach_hang/danh_sach_nxb_kh">Danh Sách Nhà Xuất Bản</a></li>
    <li><a href="khach_hang/danh_sach_tac_gia_kh">Danh Sách Tác giả</a></li>
    <li><a href="khach_hang/danh_sach_nxb_kh">Danh Sách Nhà Xuất Bản</a></li>
    
    <!-- <li><a href="khach_hang/danh_sach_doc_gia_kh">DS Độc Giả</a></li>
    <li><a href="khach_hang/danh_sach_dat_muon_kh">DS đặt mượn</a></li> -->

   <!--  <li><a href="login">Đăng Nhập</a></li>
   <li><a href="#">Đăng Ký</a></li>
   <li><a href="{{url('logout')}}">Đăng Xuất</a></li> -->
  </ul>
</div>
</body>
</html>