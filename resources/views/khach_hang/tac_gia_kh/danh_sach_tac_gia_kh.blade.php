@extends('khach_hang.layout.index')

@section('content')
        <div class="row" >
            <div class="col-lg-12">
                   <marquee>  <div class="col-lg-12">
                        <h1 class="page-header" align="center" style="color:#000066">Danh Sách Tác Giả
                            
                        </h1>
                    </div>
                </marquee>
                    
                   <div align="center">
                    <form style="width: 85%"  >
                    
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center" style="background-color:#FFCC66">
                               
                                <th>Tên Tác Giả</th>   
                                <th>Giới Thiệu</th>      
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($tac_gia as $tg)
                            <tr class="even gradeC" align="center" style="background-color: #FFCC99">
                                
                                <td>{{$tg->ten_tac_gia}}</td>
                                <td>{{$tg->gioi_thieu}}</td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection