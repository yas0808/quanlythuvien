@extends('khach_hang.layout.index')

@section('content')
 <!-- Page Content -->
       
                <div class="row" >
                    <div class="col-lg-12">
                        <h1 class="page-header" align="center" style="color: #FF6600">Đặt Mượn
                            <small style="color: #FF9933">Danh Sách Đặt Mượn</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                   
                   
                    <div align="center">
                    <form style="width: 95%; height: 400px"  >
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                               
                                <th>Tên Sách</th>
                                <th>Tên Độc Giả</th>
                                <th>Ngày Đặt</th>
                                <th>Trạng Thái</th>
                                <th>Số Lượng</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($dat_muon as $dm)
                            <tr class="even gradeC" align="center">
                                
                                <td>{{$dm->sach->ten_sach}}</td>
                                <td>{{$dm->doc_gia->ten_doc_gia}}</td>
                                <td>{{$dm->ngay_dat}}</td>
                                 <td>@if($dm->trang_thai== 1)
                                    {{"Đã Trả"}}
                                    @else
                                    {{"Chưa Trả"}}
                                    @endif</td>
                               <td> {{$dm->so_luong}}</td>
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection