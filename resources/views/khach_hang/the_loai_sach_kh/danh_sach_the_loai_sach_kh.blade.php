@extends('khach_hang.layout.index')

@section('content')
         <div class="row" >
                    <div class="col-lg-12">
                         <marquee> <h1 class="page-header" style="color: #000066">Danh Sách Thể Loại Sách
                        </h1></marquee>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                   
                   
                    <div align="center">
                    <form style="width: 55%; height: 400px"  >
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center" style="background-color:#FFCC66">
                                
                                <th>Tên thể loại sách</th>                             
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($the_loai_sach as $tl)
                            <tr class="even gradeC" align="center" style="background-color: #FFCC99">
                                <td>{{$tl->ten_the_loai_sach}}</td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection