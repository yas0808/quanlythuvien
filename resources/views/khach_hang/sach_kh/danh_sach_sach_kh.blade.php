@extends('khach_hang.layout.index')
@section('content')
 <!-- Page Content -->
                <div class="row">
                    <div class="col-lg-12" align="center">
                       <marquee> <h1 class="page-header" style="color: #000066">Sách Có Trong Thư Viện
                        </h1></marquee>
                    </div>
                    <br>
                    <div align="center">
                      <form style="width: 90%; height: 400px" >
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center" style="background-color:#FFCC66">
                                
                                <th>Thể loại sách</th>
                                <th>Tác Giả</th>
                                <th>Nhà Xuất Bản</th>
                                <th>Tên Sách</th>
                                <th>Ảnh</th>
                                <th>Giới Thiệu</th>
                               
                                <th>Giá Tiền</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($sach as $sh)
                            <tr class="even gradeC" align="center" style="background-color: #FFCC99">
                                <td>{{$sh->the_loai_sach->ten_the_loai_sach}}</td>
                                <td>{{$sh->tac_gia->ten_tac_gia}}</td>
                                <td>{{$sh->nha_xuat_ban['ten_nha_xuat_ban']}}</td>
                                <td>{{$sh->ten_sach}}</td>
                                <td>
                                    <img width="200px" height="200px" src="img/{{$sh->anh}}">
                                </td>
                                <td>{{$sh->gioi_thieu}}</td>
                               
                                <td>
                                    <?php echo number_format($sh->gia_tien); ?>
                                </td>
                                
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection