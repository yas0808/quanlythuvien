<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doc_gia extends Model
{
    protected $table = "doc_gia";
    protected $primaryKey = 'ma_doc_gia';
    public $timestamps = false;

    public function don_dat_muon()
    {
    	return $this->hasMany('App\don_dat_muon','ma_doc_gia','ma_dat_muon');
    }
    public function nganh()
    {
    	return $this->belongsTo('App\nganh','ma_nganh','ma_nganh');
    }
}
