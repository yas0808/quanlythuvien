<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use App\doc_gia;
use App\nganh;

class Doc_gia_kh_Controller extends Controller
{
     public function Danh_sach_doc_gia_kh()
	{
		$doc_gia = doc_gia::all();
		$nganh = nganh::all();
		// dd($doc_gia->toArray());
		return view('khach_hang/doc_gia_kh/danh_sach_doc_gia_kh',[
			'doc_gia'=> $doc_gia,
			'nganh'=> $nganh 
		]);
		
	}
	public function getLogin() {
    	return view('khach_hang/dang_nhap_khach_hang');
    }
    public function postLogin(Request $request) {
    	$rules = [
            'email' =>'required|email',
            'password' => 'required|min:8'
        ];
        $messages = [
            'email.required' => 'Email là trường bắt buộc',
            'email.email' => 'Email không đúng định dạng',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $email = $request->input('email');
            $password = $request->input('password');

            if( Auth::attempt(['email' => $email, 'password' =>$password])) {
                return redirect()->intended('/khach_hang/danh_sach_doc_gia_kh');
            } else {
                $errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
                return redirect()->back()->withInput()->withErrors($errors);
            }
        }
    }
}
