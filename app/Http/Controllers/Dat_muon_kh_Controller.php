<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dat_muon;
use App\doc_gia;
use App\sach;

class Dat_muon_kh_Controller extends Controller
{
    public function Danh_sach_dat_muon_kh()
	{
		$dat_muon= dat_muon::all();
		return view('khach_hang/dat_muon_kh/danh_sach_dat_muon_kh', ['dat_muon'=> $dat_muon]);
	}
}
