<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\nha_xuat_ban;
class Nha_xuat_ban_kh_Controller extends Controller
{
     public function Danh_sach_nxb_kh()
	{
		$nha_xuat_ban = nha_xuat_ban::all();
		return view('khach_hang/nha_xuat_ban_kh/danh_sach_nxb_kh', ['nha_xuat_ban'=> $nha_xuat_ban]);
	}
}
