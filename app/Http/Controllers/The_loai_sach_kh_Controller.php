<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\the_loai_sach;
class The_loai_sach_kh_Controller extends Controller
{
     public function Danh_sach_tls_kh()
	{
		$the_loai_sach= the_loai_sach::all();
		return view('khach_hang/the_loai_sach_kh/danh_sach_the_loai_sach_kh', ['the_loai_sach'=> $the_loai_sach]);
	}
	public function Trang_chu()
	{
		return view('pages/trang_chu');
	}
};
