<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tac_gia;

class Tac_gia_kh_Controller extends Controller
{
     public function Danh_sach_tac_gia_kh()
	{
		$tac_gia= tac_gia::all();
		return view('khach_hang/tac_gia_kh/danh_sach_tac_gia_kh', ['tac_gia'=> $tac_gia]);
	}
}
