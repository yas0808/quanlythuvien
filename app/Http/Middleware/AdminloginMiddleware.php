<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;


class AdminloginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
           $admin = Auth::user();
           if($admin ->cap_do == 1)
           {
             return $next($request);
           }
         else
          return redirect('admin/dang_nhap_admin');
        }
        else return redirect('admin/dang_nhap_admin');

    }
}
